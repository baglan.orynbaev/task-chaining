﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskChaining
{
    public class Program
    {
        static Random random = new Random();

        static void Main(string[] args)
        {
            var task1 = CreateArray();
            var task2 = task1.ContinueWith(antecedent => MultiplyArray(antecedent.Result, random.Next(100)));
            var task3 = task2.ContinueWith(antecedent => SortArray(antecedent.Unwrap().Result));
            var task4 = task3.ContinueWith(antecedent => CalculateAverage(antecedent.Unwrap().Result));

            Console.WriteLine(task4.Unwrap().Result);
            Console.ReadLine();
        }

        public static Task<int[]> CreateArray()
        {
            return Task.Run(() =>
            {
                int[] arr = new int[10];
                for (int i = 0; i < 10; i++)
                {
                    arr[i] = random.Next(100);
                    Console.Write(arr[i] + ", ");
                }

                Console.WriteLine();

                return arr;
            });
        }

        public static Task<int[]> MultiplyArray(int[] arr, int mult)
        {
            if (arr == null)
            {
                throw new ArgumentNullException(nameof(arr));
            }

            if (arr.Length == 0)
            {
                throw new ArgumentException("array cannot be empty", nameof(arr));
            }

            return Task.Run(() =>
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] *= mult;
                    Console.Write(arr[i] + ", ");
                }

                Console.WriteLine();

                return arr;
            });
        }

        public static Task<int[]> SortArray(int[] arr)
        {
            if (arr == null)
            {
                throw new ArgumentNullException(nameof(arr));
            }

            if (arr.Length == 0)
            {
                throw new ArgumentException("array cannot be empty", nameof(arr));
            }

            return Task.Run(() =>
            {
                Array.Sort(arr);

                for (int i = 0; i < arr.Length; i++)
                {
                    Console.Write(arr[i] + ", ");
                }

                Console.WriteLine();

                return arr;
            });
        }

        public static Task<int> CalculateAverage(int[] arr)
        {
            if (arr == null)
            {
                throw new ArgumentNullException(nameof(arr));
            }

            if (arr.Length == 0)
            {
                throw new ArgumentException("array cannot be empty", nameof(arr));
            }

            return Task.Run(() =>
            {
                int sum = 0;
                for (int i = 0; i < arr.Length; i++)
                {
                    sum += arr[i];
                }

                sum /= arr.Length;

                return sum;
            });
        }
    }
}
