using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TaskChaining;

namespace TaskChainingTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void MultiplyArrayThrowArgumentNullException()
        {
            Assert.ThrowsException<ArgumentNullException>(() => Program.MultiplyArray(null, 1));
        }

        [TestMethod]
        public void MultiplyArrayTTest()
        {
            var input = new int[]
            {
                10, 20, 30, 40, 50, 60, 70, 80, 90, 100
            };
            var task = Program.MultiplyArray(input, 5);
            var actual = task.Result;

            var expected = new int[]
            {
                50, 100, 150, 200, 250, 300, 350, 400, 450, 500
            };

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SortArrayThrowArgumentNullException()
        {
            Assert.ThrowsException<ArgumentNullException>(() => Program.SortArray(null));
        }

        [TestMethod]
        public void SortArrayTest()
        {
            var input = new int[]
            {
                100, 90, 80, 70, 60, 50, 40, 30, 20, 10
            };
            var task = Program.SortArray(input);
            var actual = task.Result;

            var expected = new int[]
            {
                10, 20, 30, 40, 50, 60, 70, 80, 90, 100
            };

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CalculateAverageThrowArgumentNullException()
        {
            Assert.ThrowsException<ArgumentNullException>(() => Program.CalculateAverage(null));
        }

        [TestMethod]
        public void CalculateAverageTest()
        {
            var input = new int[]
            {
                100, 90, 80, 70, 60, 50, 40, 30, 20, 10
            };
            var task = Program.CalculateAverage(input);
            var actual = task.Result;

            var expected = 55;

            Assert.AreEqual(expected, actual);
        }
    }
}
